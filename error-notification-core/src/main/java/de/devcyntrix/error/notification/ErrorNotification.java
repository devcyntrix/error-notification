package de.devcyntrix.error.notification;

import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Data
public class ErrorNotification {

    @Nullable
    private final Context context;
    @NotNull
    private final Throwable throwable;
    private final long timestamp = System.currentTimeMillis();

}
