package de.devcyntrix.error.notification;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.Getter;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ErrorNotificationDispatcher extends Thread {

    private static final Gson GSON = new Gson();

    @Getter
    private final URL webHookUrl;
    private final BlockingQueue<ErrorNotification> notifications = new ArrayBlockingQueue<>(1000);

    public ErrorNotificationDispatcher(URL webHookUrl) {
        super("Error Notification Dispatcher");
        setDaemon(true);
        this.webHookUrl = webHookUrl;
    }

    @Override
    public final void run() {
        try {
            while (true) {
                ErrorNotification take = notifications.take();

                System.out.println("Dispatching...");
                try {
                    HttpURLConnection urlConnection = (HttpURLConnection) webHookUrl.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("User-Agent", "Error Notification Dispatcher");

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);

                    JsonObject embedded = new JsonObject();
                    {
                        if (take.getContext() != null) {
                            embedded.addProperty("title", "Error by " + take.getContext().getName());
                        } else {
                            embedded.addProperty("title", "Error");
                        }


                        embedded.addProperty("description", "Caught a " + take.getThrowable().getClass().getCanonicalName());

                        embedded.addProperty("color", 0x9c1a11);

                        JsonArray fields = new JsonArray();

                        if (take.getThrowable().getMessage() != null) {
                            JsonObject field = new JsonObject();
                            field.addProperty("name", "Message");
                            field.addProperty("value", take.getThrowable().getMessage());
                            field.addProperty("inline", true);
                            fields.add(field);
                        }
                        {
                            StringWriter writer = new StringWriter();
                            take.getThrowable().printStackTrace(new PrintWriter(writer, true));
                            writer.close();

                            JsonObject field = new JsonObject();
                            field.addProperty("name", "Stacktrace");
                            field.addProperty("value", writer.toString());
                            field.addProperty("inline", false);
                            fields.add(field);
                        }

                        embedded.add("fields", fields);

                    }
                    JsonObject object = new JsonObject();
                    JsonArray embedsArray = new JsonArray();
                    embedsArray.add(embedded);
                    object.add("embeds", embedsArray);

                    if (take.getContext() != null)
                        take.getContext().handle(object);

                    urlConnection.connect();

                    OutputStream outputStream = urlConnection.getOutputStream();
                    OutputStreamWriter writer = new OutputStreamWriter(outputStream);
                    GSON.toJson(object, writer);
                    writer.flush();
                    writer.close();

                    urlConnection.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException ignored) {}
    }

    public boolean dispatch(ErrorNotification notification) {
        return this.notifications.offer(notification);
    }
}
