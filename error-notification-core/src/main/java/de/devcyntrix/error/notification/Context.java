package de.devcyntrix.error.notification;

import com.google.gson.JsonObject;

public interface Context {

    String getName();

    default void handle(JsonObject object) {
    }
}
