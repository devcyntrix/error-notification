package de.devcyntrix.error.notification;

import java.net.MalformedURLException;
import java.net.URL;

public class Main {

    public static void main(String[] args) {

        try {
            ErrorNotificationDispatcher dispatcher = new ErrorNotificationDispatcher(new URL("https://discord.com/api/webhooks/813656622958247967/F2HgOTh5zDThXeE0-KVBvK68II9ERmAJ2SukX6GeMULkKRI_4VR7vIOExV9Nufsg_ckd"));
            dispatcher.start();

            dispatcher.dispatch(new ErrorNotification(new Context() {
                @Override
                public String getName() {
                    return "Test v1.0.0";
                }
            }, new NullPointerException("HALLLLOoo")));

            Thread.sleep(10000);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
