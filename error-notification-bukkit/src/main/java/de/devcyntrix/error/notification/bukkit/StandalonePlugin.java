package de.devcyntrix.error.notification.bukkit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.devcyntrix.error.notification.ErrorNotificationDispatcher;
import lombok.Data;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;

public final class StandalonePlugin extends JavaPlugin {

    private Configuration configuration = new Configuration();
    private ErrorNotificationDispatcher dispatcher;

    @Override
    public void onEnable() {
        if (!getDataFolder().isDirectory()) {
            boolean mkdirs = getDataFolder().mkdirs();
            if (!mkdirs) {
                System.err.println("Failed to create data folder");
            }
        }

        File configFile = new File(getDataFolder(), "config.json");
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try {
            if (!configFile.isFile()) {
                FileWriter writer = new FileWriter(configFile);
                gson.toJson(this.configuration, writer);
                writer.close();
            } else {
                FileReader reader = new FileReader(configFile);
                this.configuration = gson.fromJson(reader, Configuration.class);
                reader.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            URL webHookURL = new URL(configuration.getWebHook());
            this.dispatcher = new ErrorNotificationDispatcher(webHookURL);
            this.dispatcher.start();

            Errors.setDispatcher(this.dispatcher);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        if (this.dispatcher != null) {
            this.dispatcher.interrupt();
            Errors.setDispatcher(null);
        }
    }

    @Data
    public static class Configuration {

        private String webHook = "https://discord.com/api/webhooks/id/token";

    }
}
