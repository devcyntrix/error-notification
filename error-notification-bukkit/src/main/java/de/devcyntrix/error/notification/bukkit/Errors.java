package de.devcyntrix.error.notification.bukkit;

import de.devcyntrix.error.notification.ErrorNotification;
import de.devcyntrix.error.notification.ErrorNotificationDispatcher;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

public final class Errors {

    @NotNull
    @Getter
    @Setter
    private static ErrorNotificationDispatcher dispatcher;

    public static void reportError(Plugin plugin, Throwable throwable) {
        dispatcher.dispatch(new ErrorNotification(new BukkitContext(plugin), throwable));
    }

}
