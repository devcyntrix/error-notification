package de.devcyntrix.error.notification.bukkit;

import de.devcyntrix.error.notification.Context;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

@RequiredArgsConstructor
public class BukkitContext implements Context {

    @Getter
    private final Plugin plugin;

    @Override
    public String getName() {
        PluginDescriptionFile description = plugin.getDescription();
        return description.getFullName();
    }
}
