package de.devcyntrix.error.notification.bungeecord;

import de.devcyntrix.error.notification.ErrorNotification;
import de.devcyntrix.error.notification.ErrorNotificationDispatcher;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.plugin.Plugin;

public final class Errors {

    @Getter
    @Setter
    private static ErrorNotificationDispatcher dispatcher;

    public static void reportError(Plugin plugin, Throwable throwable) {
        dispatcher.dispatch(new ErrorNotification(new BungeeCordContext(plugin), throwable));
    }

}
