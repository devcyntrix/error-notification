package de.devcyntrix.error.notification.bungeecord;

import de.devcyntrix.error.notification.Context;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginDescription;

@RequiredArgsConstructor
public class BungeeCordContext implements Context {

    @Getter
    private final Plugin plugin;

    @Override
    public String getName() {
        PluginDescription description = plugin.getDescription();
        return description.getName() + (description.getVersion() != null ? " v" + description.getVersion() : "");
    }
}
